## starting of script

color='\033[0;32m'
color2='\033[0;31m'
nc='\033[0m'

echo -e "${color}Starting environment setup${nc}"
PATH=~/bin:$PATH
source build/envsetup.sh
export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache

#detect crdroid version based on common.mk
croot
cr_check=$(grep -n "CR_VERSION" vendor/lineage/config/common.mk | grep -Eo '^[^:]+')
array=( $cr_check )
crDroid=$(sed -n ${array[0]}'p' < vendor/lineage/config/common.mk | cut -d "=" -f 2 | tr -d '[:space:]')

## Upload files ##
device=santoni
android=11.0
BuildID="crDroidAndroid-"$android"-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')"-"$device"-v"$crDroid".zip"

if [ -f out/target/product/santoni/$BuildID ]
then
case $1 in
    "-v"|--vendor )
        echo -e "${color}Creating vendor zip and pushing Sourceforge${nc}"
        cd out/target/product/santoni
        rm -rf vendortest
        unzip $BuildID -d vendortest
        cd vendortest
        rm -rf boot.img system.new.dat.br system.transfer.list system.patch.dat install
        sed -i "26d;27d;28d;29d;30d;31d;32d;33d;38d;39d;40d" META-INF/com/google/android/updater-script
        zip -r9 vendor-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip *
        scp -i ~/ssh-key vendor-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip jabiyeff@frs.sourceforge.net:/home/frs/project/jabiyeff-build/test/
        croot ;;
    "-vw"|--vendor-wepush )
        echo -e "${color}Creating vendor zip and pushing Wetransfer${nc}"
        cd out/target/product/santoni
        rm -rf vendortest
        unzip $BuildID -d vendortest
        cd vendortest
        rm -rf boot.img system.new.dat.br system.transfer.list system.patch.dat install
        sed -i "26d;27d;28d;29d;30d;31d;32d;33d;38d;39d;40d" META-INF/com/google/android/updater-script
        zip -r9 vendor-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip *
        wepush vendor-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip
        croot ;;
    "-vg"|--vendor-gsi )
        echo -e "${color}Creating vendor zip and pushing Sourceforge${nc}"
        cd out/target/product/santoni
        rm -rf vendortest
        unzip $BuildID -d vendortest
        cd vendortest
        rm -rf system.new.dat.br system.transfer.list system.patch.dat install
        sed -i "4d;5d;6d;7d;8d;9d;10d;11d;12d;13d;14d;15d;17d;26d;27d;28d;29d;30d;31d;32d;33d;39d" META-INF/com/google/android/updater-script
        zip -r9 vendor-gsi-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip *
        scp -i ~/ssh-key vendor-gsi-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip jabiyeff@frs.sourceforge.net:/home/frs/project/jabiyeff-build/test/
        croot ;;
    "-vgw"|--vendor-gsi-wepush )
        echo -e "${color}Creating vendor zip and pushing Wetransfer${nc}"
        cd out/target/product/santoni
        rm -rf vendortest
        unzip $BuildID -d vendortest
        cd vendortest
        rm -rf system.new.dat.br system.transfer.list system.patch.dat install
        sed -i "4d;5d;6d;7d;8d;9d;10d;11d;12d;13d;14d;15d;17d;26d;27d;28d;29d;30d;31d;32d;33d;39d" META-INF/com/google/android/updater-script
        zip -r9 vendor-gsi-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip *
        wepush vendor-gsi-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip
        croot ;;
esac
else
echo -e "${color2}Failed to create vendor${nc}"
fi

## starting of script

color='\033[0;32m'
color2='\033[0;31m'
nc='\033[0m'

echo -e "${color}Starting environment setup${nc}"
PATH=~/bin:$PATH
source build/envsetup.sh
export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache

echo -e "${color}Updating local manifests${nc}"
cd .repo/local_manifests
git pull
croot

case $1 in
    "-s"|--sync )
        echo -e "${color}Syncing sources${nc}"
        repo sync -j$(nproc --all) -c --force-sync --no-clone-bundle --no-tags ;;
    "-sc"|--sync-custom )
        echo -e "${color}Syncing sources + include more customization${nc}"
        repo sync -j$(nproc --all) -c --force-sync --no-clone-bundle --no-tags
        cd frameworks/base
        git fetch https://github.com/Jabiyeff/android_frameworks_base 11.0
        git cherry-pick 29ae475381b98b6725ca57e90017a63a101b9313
        croot
        cd packages/services/Telephony
        git fetch https://github.com/Jabiyeff/android_packages_services_Telephony 11.0
        git cherry-pick 837f465700891849bd20a42f02ef4afcc2f6ac5d
        croot
        cd vendor/qcom/opensource/commonsys/packages/apps/Bluetooth
        git fetch https://github.com/crdroidandroid/android_vendor_qcom_opensource_packages_apps_Bluetooth 11.0
        git cherry-pick 4c32ee0f07de724a8f5bf9fb7a33f86db22426de
        croot ;;
    "-sd"|--sync-device )
        echo -e "${color}Syncing device sources${nc}"
        repo sync --force-sync device-santoni vendor-santoni -j16 ;;
    "-n"|--nosync )
        echo -e "${color}Sync abandoned${nc}" ;;
esac

case $2 in
    "-c" )
        echo -e "${color}Prepare to start build${nc}"
        lunch lineage_santoni-user
        echo -e "${color}Prepare for clean build${nc}"
        rm -rf out
        echo -e "${color}Starting build${nc}"
        mka bacon ;;
    "-d" )
        echo -e "${color}Prepare to start build${nc}"
        lunch lineage_santoni-user
        echo -e "${color}Prepare for dirty build${nc}"
        make installclean
        echo -e "${color}Starting build${nc}"
        mka bacon ;;
    "-ce" )
        echo -e "${color}Prepare to start eng build${nc}"
        lunch lineage_santoni-userdebug
        echo -e "${color}Prepare for clean build${nc}"
        rm -rf out
        echo -e "${color}Starting build${nc}"
        mka bacon ;;
    "-de" )
        echo -e "${color}Prepare to start eng build${nc}"
        lunch lineage_santoni-userdebug
        echo -e "${color}Prepare for dirty build${nc}"
        make installclean
        echo -e "${color}Starting build${nc}"
        mka bacon ;;
    "-cwb" )
        echo -e "${color}Prepare to start build${nc}"
        lunch lineage_santoni-user
        echo -e "${color}Prepare for dirty build${nc}"
        rm -rf out ;;
    "-dwb" )
        echo -e "${color}Prepare to start build${nc}"
        lunch lineage_santoni-user
        echo -e "${color}Prepare for dirty build${nc}"
        make installclean ;;
esac

#detect crdroid version based on common.mk
croot
cr_check=$(grep -n "CR_VERSION" vendor/lineage/config/common.mk | grep -Eo '^[^:]+')
array=( $cr_check )
crDroid=$(sed -n ${array[0]}'p' < vendor/lineage/config/common.mk | cut -d "=" -f 2 | tr -d '[:space:]')

## Upload files ##
device=santoni
android=11.0
BuildID="crDroidAndroid-"$android"-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')"-"$device"-v"$crDroid".zip"
BuildID2="crDroidAndroid-"$android"-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')"-"$device"-v"$crDroid"-GAPPS.zip"
BuildID3="crDroidAndroid-"$android"-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')"-"$device"-v"$crDroid"-2.zip"

if [ -f out/target/product/santoni/$BuildID ]
then
case $3 in
    "-t"|--upload-test )
        echo -e "${color}Upload test build from Sourceforge${nc}"
        scp -i ~/ssh-key out/target/product/santoni/$BuildID jabiyeff@frs.sourceforge.net:/home/frs/project/jabiyeff-build/test/ ;;
    "-t2"|--upload-test2 )
        echo -e "${color}Upload test build from Sourceforge${nc}"
        mv out/target/product/santoni/$BuildID out/target/product/santoni/$BuildID3
        scp -i ~/ssh-key out/target/product/santoni/$BuildID3 jabiyeff@frs.sourceforge.net:/home/frs/project/jabiyeff-build/test/ ;;
    "-tg"|--upload-test-gapps )
        echo -e "${color}Upload test build (Non-Treble) from Sourceforge${nc}"
        mv out/target/product/santoni/$BuildID out/target/product/santoni/$BuildID2
        scp -i ~/ssh-key out/target/product/santoni/$BuildID2 jabiyeff@frs.sourceforge.net:/home/frs/project/jabiyeff-build/test/ ;;
    "-sr"|--upload-sf-release )
        echo -e "${color}Upload release build from my Sourceforge${nc}"
        scp -i ~/ssh-key out/target/product/santoni/$BuildID jabiyeff@frs.sourceforge.net:/home/frs/project/jabiyeff-build/crDroid/ ;;
    "-rg"|--upload-release-gapps )
        echo -e "${color}Upload release build (Non-Treble) from my Sourceforge${nc}"
        mv out/target/product/santoni/$BuildID out/target/product/santoni/$BuildID2
        scp -i ~/ssh-key out/target/product/santoni/$BuildID2 jabiyeff@frs.sourceforge.net:/home/frs/project/jabiyeff-build/crDroid/ ;;
    "-we"|--upload-we )
        echo -e "${color}Upload test build from Wetransfer${nc}"
        wepush out/target/product/santoni/$BuildID ;;
    "-o"|--upload-osdn )
        echo -e "${color}Upload test build from OSDN${nc}"
        scp -i ~/ssh-key out/target/product/santoni/$BuildID jabiyeff@storage.osdn.net:/storage/groups/j/ja/jabiyeff-build/ ;;
    "-r"|--upload-release )
        echo -e "${color}Upload release build from Sourceforge${nc}"
        curl --ssl -k -T out/target/product/santoni/$BuildID ftp://upme.crdroid.net/files/santoni/7.x/ --user uploader:uploader ;;
    "-v"|--vendor )
        echo -e "${color}Creating vendor zip and pushing Sourceforge${nc}"
        cd out/target/product/santoni
        rm -rf vendortest
        unzip $BuildID -d vendortest
        cd vendortest
        rm -rf boot.img system.new.dat.br system.transfer.list system.patch.dat install
        sed -i "26d;27d;28d;29d;30d;31d;32d;33d;38d;39d;40d" META-INF/com/google/android/updater-script
        zip -r9 vendor-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip *
        scp -i ~/ssh-key vendor-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip jabiyeff@frs.sourceforge.net:/home/frs/project/jabiyeff-build/test/
        croot ;;
    "-vw"|--vendor-wepush )
        echo -e "${color}Creating vendor zip and pushing Wetransfer${nc}"
        cd out/target/product/santoni
        rm -rf vendortest
        unzip $BuildID -d vendortest
        cd vendortest
        rm -rf boot.img system.new.dat.br system.transfer.list system.patch.dat install
        sed -i "26d;27d;28d;29d;30d;31d;32d;33d;38d;39d;40d" META-INF/com/google/android/updater-script
        zip -r9 vendor-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip *
        wepush vendor-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip
        croot ;;
    "-vg"|--vendor-gsi )
        echo -e "${color}Creating vendor zip and pushing Sourceforge${nc}"
        cd out/target/product/santoni
        rm -rf vendortest
        unzip $BuildID -d vendortest
        cd vendortest
        rm -rf system.new.dat.br system.transfer.list system.patch.dat install
        sed -i "4d;5d;6d;7d;8d;9d;10d;11d;12d;13d;14d;15d;17d;26d;27d;28d;29d;30d;31d;32d;33d;39d" META-INF/com/google/android/updater-script
        zip -r9 vendor-gsi-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip *
        scp -i ~/ssh-key vendor-gsi-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip jabiyeff@frs.sourceforge.net:/home/frs/project/jabiyeff-build/test/
        croot ;;
    "-vgw"|--vendor-gsi-wepush )
        echo -e "${color}Creating vendor zip and pushing Wetransfer${nc}"
        cd out/target/product/santoni
        rm -rf vendortest
        unzip $BuildID -d vendortest
        cd vendortest
        rm -rf system.new.dat.br system.transfer.list system.patch.dat install
        sed -i "4d;5d;6d;7d;8d;9d;10d;11d;12d;13d;14d;15d;17d;26d;27d;28d;29d;30d;31d;32d;33d;39d" META-INF/com/google/android/updater-script
        zip -r9 vendor-gsi-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip *
        wepush vendor-gsi-"$(date -d "$D" '+%Y')$(date -d "$D" '+%m')$(date -d "$D" '+%d')".zip
        croot ;;
esac
else
echo -e "${color2}Failed to upload files${nc}"
fi

case $4 in
    "-p"|--poweroff )
        sudo shutdown -P +10 ;;
esac
